import React from 'react'
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import CustomNavigationButton from '../components/CustomNavigationButton';
import Crud from './../db/Crud'

class CreateCardPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			langue: '',
			thematique: '',
			mot: '',
			traduction: ''
		};
	}

	addDefinition = () => {

		const language = this.state.langue;
		const content = this.state.mot;
		const translation = this.state.traduction;
		const thematique = this.state.thematique;

		Crud.addValue('definitions', {language, thematique, content, translation})
	};

	render() {


		return (
			<View style={styles.inputContainer}>
				<View style={styles.BackButton}>
					<CustomNavigationButton navigation={this.props.navigation} linkTo={"MyPage"} title="Back to My Page"
											color="white"/>
				</View>

				<TextInput
					style={styles.textInput}
					placeholder="Langue"
					maxLength={20}
					onChangeText={(langue) => this.setState({langue})}
					value={this.state.langue}
				/>
				<TextInput
					style={styles.textInput}
					placeholder="Thématique"
					maxLength={20}
					onChangeText={(thematique) => this.setState({thematique})}
					value={this.state.thematique}
				/>
				<TextInput
					style={styles.textInput}
					placeholder="Mot"
					maxLength={20}
					onChangeText={(mot) => this.setState({mot})}
					value={this.state.mot}
				/>
				<TextInput
					style={styles.textInput}
					placeholder="Traduction"
					maxLength={20}
					onChangeText={(traduction) => this.setState({traduction})}
					value={this.state.traduction}
				/>

				<Text style={{
					fontSize: 20,
					fontWeight: 'bold',
					paddingTop: 15,
					justifyContent: 'center',
					textAlign: 'center'
				}}>Définition</Text>
				<View style={styles.Box}>

					<View style={styles.BoxContainer}>
						<Text>{this.state.mot}</Text>
					</View>

					<View style={styles.BoxContainer}>
						<Text>{this.state.traduction}</Text>
					</View>

				</View>
				<TouchableOpacity style={styles.saveButton} onPress={this.addDefinition}>
					<Text style={styles.saveButtonText}>Save</Text>
				</TouchableOpacity>

			</View>
		)
	}

}


const styles = StyleSheet.create({
	BackButton: {
		paddingBottom: 50
	},
	inputContainer: {
		paddingTop: 15
	},
	Box: {
		alignContent: 'center',
		flexDirection: 'row',
		margin: 20,
		justifyContent: 'center',
	},
	BoxContainer: {

		alignContent: 'center',
		flexDirection: 'row',
		width: 50,
		height: 50,
		backgroundColor: 'powderblue',
		margin: 20,
		justifyContent: 'center',
		textAlign: 'center'
	},
	textInput: {
		borderColor: '#CCCCCC',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		height: 50,
		fontSize: 25,
		paddingLeft: 20,
		paddingRight: 20
	},
	saveButton: {
		margin: 120,
		borderWidth: 1,
		borderColor: '#007BFF',
		backgroundColor: '#007BFF',
		padding: 15,
		// margin: 5
	},
	saveButtonText: {
		color: '#FFFFFF',
		fontSize: 20,
		textAlign: 'center'
	}
});
export default CreateCardPage

