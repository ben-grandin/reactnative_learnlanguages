import React from 'react'
import {View} from 'react-native';
import ThemeCardsCreator from './../components/ThemeCardsCreator'
import Crud from "../db/Crud.js"

class HomePage extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			languages: [],
		};

		Crud.getValues("languages").then(languages => {
			this.setState({languages})
		});
	}

	render() {
		const {navigation} = this.props;

		return (
			<View>
				<ThemeCardsCreator languages={this.state.languages} navigation={navigation}/>
			</View>
		)
	}

}

export default HomePage