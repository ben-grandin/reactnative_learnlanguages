import {db} from "./firebase.config";
import * as firebase from "firebase";

export default class Crud {


	static transfromToArray = function (snap) {
		let totos = [];
		snap.forEach(item => {
			const itemVal = item.val();
			totos.push(itemVal)
		});
		return totos;
	};

	static async getValue(collections, id) {
		return firebase.database().ref(`/${collections}/${id}`).once('value')
	}

	static async getValues(collections, {key, value} = {}) {

		let values = [];

		if (arguments[1]) {

			console.log("arguments[1]");
			console.log(arguments[1]);

			await firebase.database().ref(`/${collections}`).orderByChild(`/${key}`).equalTo(value).once("value", snap => {
				values = this.transfromToArray(snap)
			});
		} else {
			await firebase.database().ref(`/${collections}`).once('value', snap => {
				values = this.transfromToArray(snap)
			})
		}
		return values;
	}

	static async addValue(collections, {id, name, language, thematique, content, translation}) {

		let objToAdd;
		switch (collections) {
			case "themes":
				objToAdd = {
					name,
					language
				};
				break;
			case "definitions":
				objToAdd = {
					language,
					thematique,
					content,
					translation
				};
				break;
			case "languages":
				objToAdd = {
					name
				};
				break

		}

		if (id) {
			db.ref(`/${collections}${id ? "/" + id : ""}`).set(objToAdd);
		} else {
			db.ref(`/${collections}`).push(objToAdd);
		}
	};


}