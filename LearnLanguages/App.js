import React from 'react';

import LoginPage from './pages/LoginPage'
import CreateAccountPage from './pages/CreateAccountPage'
import HomePage from './pages/HomePage'
import ExercicePage from './pages/ExercicePage'
import MyPage from './pages/MyPage'
import CreateCardPage from './pages/CreateCardPage'
import ThemesPage from './pages/ThemesPage'
import CardsPage from './pages/CardsPage'
import ReglagesPage from './pages/ReglagesPage'
import ShowThemesPage from './pages/ShowThemesPage'
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';


const AppNavigator = createStackNavigator(
	{
    Login: LoginPage,
    CreateAccount: CreateAccountPage,
    Home: HomePage,
    Exercice: ExercicePage,
    MyPage: MyPage,
    CreateCard: CreateCardPage,
    Themes: ThemesPage,
    Cards: CardsPage,
	Reglages: ReglagesPage,
	ShowThemes: ShowThemesPage
	},
	{
		initialRouteName: 'Login',
		defaultNavigationOptions: {
			title: "LearnLanguages",
      
			headerStyle: {
				backgroundColor: '#000000',
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				fontWeight: 'bold',
			},
		},
	}
);
const AppContainer = createAppContainer(AppNavigator);


export default class App2 extends React.Component {
	render() {
		return <AppContainer/>;
	}
}
